void main() {
  var appOfYearWinners = [
    "FNB-Banking",
    "Plascon",
    "Matchy",
    "PhrazApp",
    "HealthID",
    "Transunion-Dealers-Guide",
    "Rapid-Targets",
    "bookly",
    "deloitte-digital",
    "dstv",
    "gautrain-buddy",
    "kids-aid",
    "markitshare",
    "nedbank",
    "price-check",
    "snapscan",
    "live-inspect",
    "my-belongings",
    "rea-vaya",
    "supersport",
    "sync-mobile",
    "vigo",
    "wildlife-tracker",
    "zapper",
    "cput-mobile",
    "dstv-now",
    "eskomsepush",
    "m4jam",
    "vula-mobile",
    "wumdrop",
    "domestly",
    "friendly-math-monsters-for-kindergarten",
    "hearza",
    "ikhokha",
    "kaching",
    "mibrand",
    "tuta-me",
    "digger",
    "si-realities",
    "vula-mobile2",
    "hydra-farm",
    "matric-live",
    "franc",
    "over",
    "loctransi",
    "naked-insurance",
    "my-pregnancy-journey",
    "loot-defence",
    "mowash",
    "checkers-sixty-sixty",
    "easy-equities",
    "birdpro",
    "technishen",
    "bottles",
    "lexie-hearing",
    "league-of-legends",
    "examsta",
    "xitsonga-dictionary",
    "greenfingers-mobile",
    "guardian-health",
    "Ambani-Afrika",
    "Takealot",
    "Shytft",
    "iiDENTIFii",
    "SISA",
    "Guardian-Health-Platform",
    "Murimi",
    "UniWise",
    "Kazi-App",
    "Rekindle",
    "Hellopay-SoftPOS",
    "Roadsave"
  ];

  appOfYearWinners.sort();
  print("App of the Year winners $appOfYearWinners");

  var winners2017 = [
    "Pick-n-Pays-Super-Animals",
    "Watif-Health-Portal",
    "Transunion-1check",
    "Intergreatmev",
    "Orderin",
    "Hey-Jude",
    "Oru-Social-TouchSA",
    "Awethu-Project",
    "Zulzi",
    "Shyft-for-Standard-Bank",
    "Ecoslips"
  ];

  winners2017.sort();
  print("2017 App of the Year winners $winners2017");

  var winners2018 = [
    "cowa-bunga",
    "acgl",
    "besmarta",
    "xander-english",
    "ctrl",
    "pineapple",
    "bestee",
    "stokfella",
    "asi-snakes"
  ];

  winners2018.sort();
  print("2018 App of the Year winners $winners2018");

  int count() {
    int number =
        appOfYearWinners.length + winners2017.length + winners2018.length;
    return number;
  }

  ;

  int total = count();
  print("Total number of apps : $total ");
}
