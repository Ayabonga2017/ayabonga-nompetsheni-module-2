void main() {
  var appAward = new Application();
  appAward.appName = "OrderIN".toUpperCase();
  appAward.sector = "Best Consumer Solution";
  appAward.developer = "Farrel Hardenberg";
  appAward.year = 2017;

  appAward.printAppInfo();
}

class Application {
  String? appName;
  String? sector;
  String? developer;
  int? year;

  void printAppInfo() {
    print("Application name: $appName");
    print("Application sector: $sector");
    print("Developer name: $developer");
    print("Application year: $year");
  }
}
